
import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button, TouchableOpacity,
  TextInput
} from 'react-native';


import axios from 'axios'
import Weather from './components/Weather';

const App = () => {
  const [zipCode, setzipCode] = useState('80140')
  const [tempZip , setTempZip] = useState('')
  const [text,setText] = useState('Hello World')
  const update = () => {
    setzipCode(tempZip)
  }
  const pressText = () => {
    console.log('Pressed')
    setText('Sawadee Krub')
  }
  return(
    <View style={styles.mobile}>
      <View style={styles.weather}>
        <Weather zipCode={zipCode}/>
      </View>
      <View style={{alignSelf : "center"}}>
    
        <TextInput style={{ 
          margin : 20,
          width : 200 , 
          height : 50,
          textAlign :"center" , 
          justifyContent : "center",
          borderRadius :10,
          borderWidth : 1,

        }} 
          placeholder="zipcode" 
          onChangeText={(text)=>setTempZip(text)}/>
        <Button
          title="Update"
          onPress = {()=>update()}
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  mobile : {
    marginTop : 0
  },  
  container : {
    flex : 1,
    alignItems : "center",
    justifyContent :"center",
  },
  weather : {
    textAlign : "center"
  },
  font : {
    fontSize : 24,
  },
  buttonFont : {
    color : "#ffffff",
    fontSize : 18,
  },
  inputContainer: {
    borderLeftWidth: 4,
    borderRightWidth: 4,
    height: 70
  },
  input: {
    height: 70,
    backgroundColor: '#ffffff',
    paddingLeft: 15,
    paddingRight: 15
  },

  button : {
    margin : 10,
    paddingVertical : 20,
    paddingHorizontal : 100,
    borderStyle : "solid",
    backgroundColor: '#090979',
    borderRadius : 10,

  }
})
export default App;
